#!/usr/bin/env bash

PROG=$1
DIFF="diff -sy --suppress-common-lines --left-column"

for ifile in tests/*.in; do
    ofile=`echo $ifile | sed s/.in$/.ou/g`

    echo "Running test $ifile"
    echo "-------------------------------"
    $DIFF $ofile <($PROG $ifile)
    echo ""
done
