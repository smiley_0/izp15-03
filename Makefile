CFLAGS=-Wall -Wextra -std=c99 -pedantic -DNDEBUG -Werror #-O2
LDFLAGS=-lm
MAIN=proj3
OUTFILE=$(MAIN).out

DOCS=doxygen
DOXYFILE=Doxyfile

$(OUTFILE): $(MAIN).c
	$(CC) $(CFLAGS) $(MAIN).c -o $(OUTFILE) $(LDFLAGS)
	$(DOCS) $(DOXYFILE) > /dev/null

.PHONY: clean cleandocs cleanall test debug

cleanall: clean cleandocs

clean:
	rm $(OUTFILE)

cleandocs:
	rm -rf html/ latex/

test: $(OUTFILE)
	#./runtests.sh ./$(OUTFILE)

debug: $(OUTFILE)
	lldb $(OUTFILE)
