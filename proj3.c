/**
 * IZP Project 03
 *
 * @author   Adrian Kiraly (xkiral01@stud.fit.vutbr.cz) [1BIA]
 * @date     2015-12-05
 * @version  1.1
 *
 * Jednoducha shlukova analyza: 2D nejblizsi soused.
 * Single linkage
 * http://is.muni.cz/th/172767/fi_b/5739129/web/web/slsrov.html
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h> // sqrtf
#include <limits.h> // INT_MAX
#include <string.h> // memcpy
#include <errno.h>
#include <stdbool.h>

/*****************************************************************
 * Ladici makra. Vypnout jejich efekt lze definici makra
 * NDEBUG, napr.:
 *   a) pri prekladu argumentem prekladaci -DNDEBUG
 *   b) v souboru (na radek pred #include <assert.h>
 *      #define NDEBUG
 */
#ifdef NDEBUG
#define debug(s)
#define dfmt(s, ...)
#define dint(i)
#define dfloat(f)
#else

// vypise ladici retezec
#define debug(s) printf("- %s\n", s)

// vypise formatovany ladici vystup - pouziti podobne jako printf
#define dfmt(s, ...) printf(" - " __FILE__ ":%u: " s "\n",__LINE__,__VA_ARGS__)

// vypise ladici informaci o promenne - pouziti dint(identifikator_promenne)
#define dint(i) printf(" - " __FILE__ ":%u: " #i " = %d\n", __LINE__, i)

// vypise ladici informaci o promenne typu float - pouziti
// dfloat(identifikator_promenne)
#define dfloat(f) printf(" - " __FILE__ ":%u: " #f " = %g\n", __LINE__, f)

#endif

// macros for printing error message and returning error code
#define die_with_err_fmt(code, msg, ...) do { \
    fprintf(stderr, "error: " msg "\n", __VA_ARGS__); \
    return code; \
} while (0)

#define die_with_err(code, msg) die_with_err_fmt(code, "%s", msg)

/*****************************************************************
 * Deklarace potrebnych datovych typu:
 *
 * TYTO DEKLARACE NEMENTE
 *
 *   struct obj_t - struktura objektu: identifikator a souradnice
 *   struct cluster_t - shluk objektu:
 *      pocet objektu ve shluku,
 *      kapacita shluku (pocet objektu, pro ktere je rezervovano
 *          misto v poli),
 *      ukazatel na pole shluku.
 */

struct obj_t {
    int id;
    float x;
    float y;
};

struct cluster_t {
    int size;
    int capacity;
    struct obj_t *obj;
};

/*****************************************************************
 * Deklarace potrebnych funkci.
 *
 * PROTOTYPY FUNKCI NEMENTE
 *
 * IMPLEMENTUJTE POUZE FUNKCE NA MISTECH OZNACENYCH 'TODO'
 *
 */

/*
 Inicializace shluku 'c'. Alokuje pamet pro cap objektu (kapacitu).
 Ukazatel NULL u pole objektu znamena kapacitu 0.
*/
void init_cluster(struct cluster_t *c, int cap)
{
    assert(c != NULL);
    assert(cap >= 0);

    c->obj = malloc(cap * sizeof(struct obj_t));
    c->size = 0;
    c->capacity = (c->obj != NULL) ? cap : 0;

    dfmt("Inited cluster @%p with size=%d capacity=%d, (sizeof=%lu)", c,
        c->size, c->capacity, sizeof(*c));
}

/*
 Odstraneni vsech objektu shluku a inicializace na prazdny shluk.
 */
void clear_cluster(struct cluster_t *c)
{
    free(c->obj);
    c->obj = NULL;
    c->size = 0;
    c->capacity = 0;
}

/// Chunk of cluster objects. Value recommended for reallocation.
const int CLUSTER_CHUNK = 10;

/*
 Zmena kapacity shluku 'c' na kapacitu 'new_cap'.
 */
struct cluster_t *resize_cluster(struct cluster_t *c, int new_cap)
{
    // TUTO FUNKCI NEMENTE
    assert(c);
    assert(c->capacity >= 0);
    assert(new_cap >= 0);

    if (c->capacity >= new_cap)
        return c;

    size_t size = sizeof(struct obj_t) * new_cap;

    void *arr = realloc(c->obj, size);
    if (arr == NULL)
        return NULL;

    c->obj = arr;
    c->capacity = new_cap;
    return c;
}

/*
 Prida objekt 'obj' na konec shluku 'c'. Rozsiri shluk, pokud se do nej objekt
 nevejde.
 */
void append_cluster(struct cluster_t *c, struct obj_t obj)
{
    if (c->size+1 > c->capacity)
        resize_cluster(c, c->capacity+CLUSTER_CHUNK);

    c->obj[c->size++] = obj;
}

/*
 Seradi objekty ve shluku 'c' vzestupne podle jejich identifikacniho cisla.
 */
void sort_cluster(struct cluster_t *c);

/*
 Do shluku 'c1' prida objekty 'c2'. Shluk 'c1' bude v pripade nutnosti rozsiren.
 Objekty ve shluku 'c1' budou serazny vzestupne podle identifikacniho cisla.
 Shluk 'c2' bude nezmenen.
 */
void merge_clusters(struct cluster_t *c1, struct cluster_t *c2)
{
    assert(c1 != NULL);
    assert(c2 != NULL);

    int available_cap = c1->capacity - c1->size;

    dint(c2->size);
    dint(available_cap);

    if (c2->size > available_cap)
        resize_cluster(c1, c1->capacity+c2->size+1);

    dint(c1->capacity);

    dfmt("copying to %p", &c1->obj[c1->size]);
    dfmt("c1 starts @%p", c1->obj);
    dfmt("c1 ends @%p", c1->obj+c1->size);

    memcpy(&c1->obj[c1->size], c2->obj, c2->size * sizeof(struct obj_t));
    c1->size += c2->size;

    sort_cluster(c1);
}

/**********************************************************************/
/* Prace s polem shluku */

/*
 Odstrani shluk z pole shluku 'carr'. Pole shluku obsahuje 'narr' polozek
 (shluku). Shluk pro odstraneni se nachazi na indexu 'idx'. Funkce vraci novy
 pocet shluku v poli.
*/
int remove_cluster(struct cluster_t *carr, int narr, int idx)
{
    assert(idx < narr);
    assert(narr > 0);

    clear_cluster(&carr[idx]);

    // copy last cluster to address of removed cluster, if they are not the same
    if (idx != narr-1)
        memcpy(&carr[idx], &carr[narr-1], sizeof(struct cluster_t));


    // pointless to do realloc here, makes things more complicated and the saved
    // memory is just not worth it
    return narr-1;
}

/*
 Pocita Euklidovskou vzdalenost mezi dvema objekty.
 */
float obj_distance(struct obj_t *o1, struct obj_t *o2)
{
    assert(o1 != NULL);
    assert(o2 != NULL);

    float dx = fabs(o2->x - o1->x);
    float dy = fabs(o2->y - o1->y);

    return sqrtf(dx*dx + dy*dy);
}

/*
 Pocita vzdalenost dvou shluku. Vzdalenost je vypoctena na zaklade nejblizsiho
 souseda.
*/
float cluster_distance(struct cluster_t *c1, struct cluster_t *c2)
{
    assert(c1 != NULL);
    assert(c1->size > 0);
    assert(c2 != NULL);
    assert(c2->size > 0);

    dfmt("comparing c1 @%p vs c2 @%p", c1, c2);

    float min_dist = INFINITY;
    for (int i = 0; i < c1->size; ++i)
    {
        for (int j = 0; j < c2->size; ++j)
        {
            min_dist = fmin(obj_distance(&c1->obj[i], &c2->obj[j]), min_dist);
            dfmt("c1[%d] to c2[%d] min_dist=%f", i, j, min_dist);
        }
    }

    return min_dist;
}

/*
 Funkce najde dva nejblizsi shluky. V poli shluku 'carr' o velikosti 'narr'
 hleda dva nejblizsi shluky (podle nejblizsiho souseda). Nalezene shluky
 identifikuje jejich indexy v poli 'carr'. Funkce nalezene shluky (indexy do
 pole 'carr') uklada do pameti na adresu 'c1' resp. 'c2'.
*/
void find_neighbours(struct cluster_t *carr, int narr, int *c1, int *c2)
{
    assert(narr > 0);

    float min_dist = INFINITY;
    for (int i = 0; i < narr; ++i)
    {
        for (int j = i+1; j < narr; ++j)
        {
            float dist = cluster_distance(&carr[i], &carr[j]);
            if (dist < min_dist)
            {
                min_dist = dist;
                *c1 = i;
                *c2 = j;
            }
        }
    }
}

// pomocna funkce pro razeni shluku
static int obj_sort_compar(const void *a, const void *b)
{
    // TUTO FUNKCI NEMENTE
    const struct obj_t *o1 = a;
    const struct obj_t *o2 = b;
    if (o1->id < o2->id) return -1;
    if (o1->id > o2->id) return 1;
    return 0;
}

/*
 Razeni objektu ve shluku vzestupne podle jejich identifikatoru.
*/
void sort_cluster(struct cluster_t *c)
{
    // TUTO FUNKCI NEMENTE
    qsort(c->obj, c->size, sizeof(struct obj_t), &obj_sort_compar);
}

/*
 Tisk shluku 'c' na stdout.
*/
void print_cluster(struct cluster_t *c)
{
    // TUTO FUNKCI NEMENTE
    for (int i = 0; i < c->size; i++)
    {
        if (i) putchar(' ');
        printf("%d[%g,%g]", c->obj[i].id, c->obj[i].x, c->obj[i].y);
    }
    putchar('\n');
}

/*
 Ze souboru 'filename' nacte objekty. Pro kazdy objekt vytvori shluk a ulozi
 jej do pole shluku. Alokuje prostor pro pole vsech shluku a ukazatel na prvni
 polozku pole (ukalazatel na prvni shluk v alokovanem poli) ulozi do pameti,
 kam se odkazuje parametr 'arr'. Funkce vraci pocet nactenych objektu (shluku).
 V pripade nejake chyby uklada do pameti, kam se odkazuje 'arr', hodnotu NULL.
*/
int load_clusters(char *filename, struct cluster_t **arr)
{
    assert(arr != NULL);

    *arr = NULL;    // assume fail by default

    FILE *f = fopen(filename, "r");
    if (f == NULL)
    {
        // unable to open file
        perror(filename);
        return 0;
    }

    int obj_count;
    if (fscanf(f, "count=%d", &obj_count) != 1)
    {
        // syntax err on first line
        errno = -242;
        fprintf(stderr, "%s: syntax error\n", filename);
        fclose(f);
        return 0;
    }

    *arr = malloc(obj_count * sizeof(struct cluster_t));
    if (*arr == NULL)
    {
        // malloc failed
        perror("proj3");
        fclose(f);
        return 0;
    }

    for (int i = 0; i < obj_count; ++i)
    {
        init_cluster(&(*arr)[i], 1);
        struct obj_t obj;

        if (fscanf(f, "%d %f %f", &obj.id, &obj.x, &obj.y) != 3)
        {
            // syntax error on subsequent lines
            errno = -242;
            fprintf(stderr, "%s: syntax error\n", filename);
            fclose(f);
            return i+1;
        }
        append_cluster(&(*arr)[i], obj);
    }

    fclose(f);
    return obj_count;
}

/*
 Tisk pole shluku. Parametr 'carr' je ukazatel na prvni polozku (shluk).
 Tiskne se prvnich 'narr' shluku.
*/
void print_clusters(struct cluster_t *carr, int narr)
{
    printf("Clusters:\n");
    for (int i = 0; i < narr; i++)
    {
        printf("cluster %d: ", i);
        print_cluster(&carr[i]);
    }
}

int main(int argc, char *argv[])
{
    struct cluster_t *clusters;

    // not enough arguments
    if (argc < 2)
        die_with_err(42, "no file specified");

    int max_clusters = 1;

    // cluster count specified
    if (argc > 2)
    {
        char *end = NULL;
        long t = strtol(argv[2], &end, 10);

        if (*end == 0 && t > 0 && t < INT_MAX)
            max_clusters = t;
        else
            die_with_err(47, "invalid cluster count");
    }

    errno = 0;
    int cl_count = load_clusters(argv[1], &clusters);

    // non-zero errno breaks cycle with error
    while (cl_count > max_clusters && errno == 0)
    {
        int c1, c2;
        find_neighbours(clusters, cl_count, &c1, &c2);
        merge_clusters(&clusters[c1], &clusters[c2]);
        cl_count = remove_cluster(clusters, cl_count, c2);
    }

    if (errno == 0)
        print_clusters(clusters, cl_count);

    for (int i = 0; i < cl_count; ++i)
        clear_cluster(&(clusters[i]));

    free(clusters);

    if (errno != 0)
        return abs(errno);

    return EXIT_SUCCESS;
}
