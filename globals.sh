#!/bin/sh

BINARY="$1"
[ -z "$BINARY" ] && BINARY=proj3

die() {
    echo "chyba: $@" >&2
    echo "pouziti: $0 jmeno_binarniho_souboru" >&2
    exit 1
}

[ -f "$BINARY" ] || die "nenasel jsem soubor $BINARY"

symbols=`nm -l $BINARY|grep ' [TBDR] '|egrep -v '( _|GLIBC)'|
    sed 's/.* .  //' | sed 's|/.*/||'|awk '{print $4 ": " $3}'`

allow="init_cluster
clear_cluster
CLUSTER_CHUNK
resize_cluster
append_cluster
sort_cluster
merge_cluster
remove_cluster
obj_distance
cluster_distance
find_neighbours
obj_sort_compar
sort_cluster
print_cluster
load_clusters
print_clusters
main"

ere=`echo -n "$allow"|tr '\n' '|'`

nezname=`echo "$symbols" | egrep -v "($ere)"`
if [ -z "$nezname" ]; then
    echo Zadne dalsi funkce nebo globalni promenne.
else
    echo "Neznamene identifikatory (globalni promenne nebo funkce):"
    echo "$nezname"
fi
